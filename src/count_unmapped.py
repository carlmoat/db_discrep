#!/usr/bin/env python

try: import ipdb
except: pass

import argparse
import json
import os
import pandas as pd

import db

pd.set_option('display.height', 1000)
pd.set_option('display.width', 1000)


def build_query_string(medium, start_date, end_date):
    brandsfile = '/home/ubuntu/v2.5/research/benchmarks/accounts/' + \
        medium + '/2015-q2.json'
    brandsblob = json.load(open(brandsfile, 'rb'))
    brands = brandsblob.keys()
    if medium == 'video':
        brands = [b.lstrip('VID_').lstrip('VID_') for b in brands]
    ubrands = ['UM' + b for b in brands]
    if medium == 'video':
        brands += ['VID_' + b for b in brands]
        ubrands += ['VID_' + b for b in ubrands]
    table = 'v' * (medium == 'video') + 'bylevel0top'
    fp = open('/home/ubuntu/db_discrep/sql/count_unmapped.sql', 'rb')
    return fp.read().strip().format(
            _end_date=end_date,
            _start_date=start_date,
            _table=table,
            _tops="','".join(brands),
            _utops="','".join(ubrands),
            )

def main(args):
    querystr = build_query_string(args.medium, args.start_date, args.end_date)
    print querystr
    cxn = db.open_connection_to_vertica()
    df = db.issue_query(querystr, cxn.cursor())
    cxn.cursor().close()
    cxn.close()
    print df.to_string(index=False)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--medium', '-m', choices=['display', 'video'], default='display')
    parser.add_argument('--start-date', '-s', default='2015-09-01')
    parser.add_argument('--end-date', '-e', default='2015-09-20')
    main(parser.parse_args())

