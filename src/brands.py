#!/usr/bin/env python

try: import ipdb
except: pass

import argparse
import json


def get_brands(medium):
    brandsfile = '/home/ubuntu/v2.5/research/benchmarks/accounts/' \
            + medium + '/2015-q2.json'
    brands = json.load(open(brandsfile, 'rb')).keys()
    if medium == 'video':
        brands = [b.lstrip('VID_').lstrip('VID_') for b in brands]
        brands += ['VID_' + b for b in brands]
    return brands

def main(args):
    print '\n'.join(get_brands(args.medium))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--medium', '-m', choices=['display', 'video'], default='display')
    main(parser.parse_args())

