#!/usr/bin/env python

import datetime
import json
import os
import sys
import urlparse


V25 = os.path.join(os.path.expanduser('~'), 'v2.5')

query_templates = {
        'prod': 'get?by=level0top&level0top={brand}&start={today}&end={today}',
        'domain': 'get?by=level4&level0top={prefix}BENCHMARK1&level4={brand}&start={today}&end={today}',
        }

def query(medium, prod_brand, bm1_brand, host=None, ports=None, date=None, **kwargs):
    loads = {k: 0 for k in ['prod', 'domain']}
    if date is None:
        today = datetime.datetime.now()
        date = today.strftime('%Y-%m-%d')
    prefix = 'VID_' if medium == 'video' else ''
    for stack, brand in zip(['prod', 'domain'], [prod_brand, bm1_brand]):
        querystr = query_templates[stack].format(brand=brand, today=date, prefix=prefix)
        print >> sys.stderr, 'Running query ', querystr
        if ports is None:
            query_obj = urlparse.urlparse(querystr)
            query_dict = dict(urlparse.parse_qsl(query_obj.query))
            if stack == 'domain':
                rsp = dataservice.query(query_dict=query_dict, timeout=120, **kwargs)
            else:
                rsp = dataservice.query(query_dict=query_dict, timeout=120)
        else:
            rsp = client.query(host, ports[stack], querystr + '\n', 20)
            rsp = json.loads(rsp[:-1])
        for item in rsp:
            loads[stack] += item.get('l', 0)
    return loads['prod'], loads['domain']

def compare_stacks(medium, outfile, host=None, ports=None, date=None, brands=None, stack_name=None):
    stacks = ['prod', 'domain']
    counts = {k: {} for k in stacks}

    fq = open(outfile, 'wb')
    q4_brands = json.load(open(os.path.join(V25, 'research', 'benchmarks', 'data', 'accounts', medium, '2015-q4.json'), 'rb'))
    domain_brands_fname = os.path.dirname(os.path.abspath(__file__)) + '/../data/{}/bm1.tops'.format(medium)
    domain_brands = sorted([line.strip() for line in open(domain_brands_fname, 'rb')])
    if brands is not None:
        domain_brands = [b for b in domain_brands if b in brands]
    brand_pairs = []
    for brand in domain_brands:
        if brand in q4_brands:
            brand_pairs.append((brand, brand))
        elif medium == 'video' and 'VID_' + brand in q4_brands:
            brand_pairs.append(('VID_' + brand, brand))

    for prod_brand, bm1_brand in brand_pairs:
        if ports is None:
            kwargs = {
                    'query_all_peergroups': True,
                    'stack': stack_name,
                    'role': 'domaindata',
                    }
        else:
            kwargs = {
                    'host': host,
                    'ports': ports,
                    'date': date,
                    }
        prod_count, domain_count = query(medium, prod_brand, bm1_brand, **kwargs)
        counts['prod'][prod_brand]= prod_count
        counts['domain'][prod_brand] = domain_count
        outline = ','.join([prod_brand] + [str(counts[stack][prod_brand]) for stack in stacks])
        if outfile != '/dev/stdout':
            print >> sys.stderr, outline
        fq.write(outline + '\n')
        fq.flush()


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('medium', choices=['display', 'video'])
    parser.add_argument('--host', '-H', default='localhost')
    parser.add_argument('--ports', '-p', nargs=2, type=int, help='If supplied, we look for local stats-caches on args.host. Else we use dataservice.')
    parser.add_argument('--date')
    parser.add_argument('--brands', '-b', nargs='+', help='For video, omit the VID_ prefix.')
    parser.add_argument('--stack', '-s', help='domaindata stack for dataservice to query.', default='frank', dest='stack_name')
    parser.add_argument('--outfile', '-o', default='/dev/stdout')
    args = parser.parse_args()

    if args.ports is None:
        sys.path.insert(0, os.path.join(V25, 'clients', 'python'))
        import dataservice
    else:
        sys.path.insert(0, os.path.join(V25, 'stats-cache'))
        import client
        args.ports = {'prod': args.ports[0], 'domain': args.ports[1]}

    compare_stacks(**vars(args))

