#!/usr/bin/env python

try: import ipdb
except: pass

import argparse
from datetime import datetime
import json
import sys

from brands import get_brands
sys.path.insert(0, '/home/ubuntu/v2.5/research/benchmarks')
from util.db import Session
from geo.src.queries import QueryBuilder


def main(args):
    start_date = args.date_range[0]
    if len(args.date_range) == 2:
        end_date = args.date_range[1]
    else:
        end_date = datetime.today().strftime('%Y-%m-%d')
    if args.device is not None:
        builder = QueryBuilder()
        device_expression = builder.get_device_expression(sep='')
        and_where_device = 'AND ' + device_expression + " = '" + args.device + "'"
    else:
        and_where_device = ''
    table_prefix = 'v' * (args.medium == 'video')
    brands = get_brands(args.medium) if args.brands is None else args.brands
    level0top_prefix = 'VID_' * (args.medium == 'video')
    tops_for_level4 = "','".join(brands)
    tops_for_level0top = "','".join([level0top_prefix + b for b in brands])
    db = Session('redshift', '/home/ubuntu/db_discrep/sql', debug=args.verbose)
    rsp = db.issue_query('counts_bm1_vs_level0top.sql',
            _and_where_device=and_where_device,
            _end_date=end_date,
            _level0top_prefix=level0top_prefix,
            _start_date=start_date,
            _table_prefix=table_prefix,
            _tops_for_level0top=tops_for_level0top,
            _tops_for_level4=tops_for_level4,
            )
    if rsp is not None:
        print rsp.to_string(index=False)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--brands', '-b', nargs='+')
    parser.add_argument('--date-range', '-d', nargs='+', default=['2015-08-07'])
    parser.add_argument('--device')
    parser.add_argument('--medium', '-m', choices=['display', 'video'], default='display')
    parser.add_argument('--verbose', '-v', action='store_true', help='If set, the formatted query string will be printed to stderr.')
    main(parser.parse_args())

