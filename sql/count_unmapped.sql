WITH
  mapped_impressions AS (
    SELECT
      level0top AS level0top_m,
      sum(l) AS l_m
    FROM statscache.{_table}
    WHERE
      level0top IN ('{_tops}')
      AND date BETWEEN '{_start_date}' AND '{_end_date}'
    GROUP BY level0top
  ),
  unmapped_impressions AS (
    SELECT
      level0top AS level0top_u,
      sum(l) AS l_u
    FROM statscache.{_table}
    WHERE
      level0top IN ('{_utops}')
      AND date BETWEEN '{_start_date}' AND '{_end_date}'
    GROUP BY level0top
  )
SELECT
  level0top_m,
  REPLACE(level0top_u, 'VID_UM', '') AS foo,
  l_m,
  level0top_u,
  l_u,
  l_u / (l_m + l_u)::float AS fraction_unmapped
FROM mapped_impressions
FULL OUTER JOIN unmapped_impressions
ON
  level0top_m = 'VID_' || REPLACE('VID_UM', '', level0top_u)
  OR level0top_u = 'UM' || level0top_m
GROUP BY level0top_m, l_m, level0top_u, l_u
ORDER BY fraction_unmapped DESC
;
