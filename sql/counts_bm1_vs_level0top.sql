WITH
  level0top_counts AS (
    SELECT
      level0top AS level0top_l,
      date,
      sum(l) AS l_l
    FROM {_table_prefix}bylevel4slicer
    WHERE
      date BETWEEN '{_start_date}' AND '{_end_date}'
      AND level0top IN ('{_tops_for_level0top}')
      {_and_where_device}
    GROUP BY level0top, date
    ORDER BY date
  ),
  benchmark1_counts AS (
    SELECT
      level4 AS level0top_b,
      date,
      sum(l) AS l_b
    FROM {_table_prefix}bylevel4slicer
    WHERE
      level0top = '{_level0top_prefix}BENCHMARK1'
      AND date BETWEEN '{_start_date}' AND '{_end_date}'
      AND level4 IN ('{_tops_for_level4}')
      {_and_where_device}
    GROUP BY level4, date
    ORDER BY date
  )
SELECT
  level0top_l AS level0top,
  t1.date AS date,
  l_l AS l_top,
  l_b AS l_bm1,
  l_b - l_l AS l_top_minus_l_bm1,
  (l_b - l_l) / l_l::float AS rel_diff
FROM level0top_counts t1
JOIN benchmark1_counts t2
ON level0top_l = '{_level0top_prefix}' || level0top_b
AND t1.date = t2.date
ORDER BY level0top, date
;

