Notes from meeting
====

Mapping errors
----

When there are errors mapping pixel fires in the production stack -- which
results in the impressions being excluded from normal Redshift data but kept
(maybe?) in the BENCHMARK1 stack -- then it might be right to go with the
latter.


Data restrictions
----

To do: Check how (if at all) data restrictions restrict which data goes into
Redshift, normal data and BENCHMARK1 data.

Data restrictions are sometimes put into effect in response to fishiness in the
data so maybe we _should_ filter it away from Redshift.


Monitoring
----

Let's set up some monitoring. Let's make use of AWS Cloudwatch.

Maybe we'll decide that impression counts aren't the best parity check. Maybe
compare the computed benchmarks instead, or additionally.

For monitoring, we could use just the, say, 50 highest volume level0tops among
those included in benchmarks computation, if otherwise the computation is too
burdensome.

Dan F has a script for checking impression count consistency between live stack
and production. Maybe crib from that. See Lucky's email.


Duration issue
----

There's an issue involving duration measurability that Lucky is in the process
of sorting out. We will have to address it somehow in computing benchmarks for
Q3 and maybe going forward.


Items from Lucky's email
====

  1. Re-deploy the benchmark stack whenever we make a new stack auth ( Do this every time we make a new stack auth) -> Lucky
  2. Create some monitoring / alerts for when the domain stack doesn't match the prod stack (something like compare_stats_cache_to_livedata.py)
  3. Compare overall Benchmark1 benchmarks to production benchmarks daily -> Carl  (v2.5/util/cloudwatch/put_cloudwatch_metric.py)
